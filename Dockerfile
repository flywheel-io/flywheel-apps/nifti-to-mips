FROM python:3.8-slim as base

ENV FLYWHEEL="/flywheel/v0"
WORKDIR ${FLYWHEEL}

# Dev install. git for pip editable install.
RUN apt-get update && apt-get install -y git && \ 
    pip install "poetry==1.0.0"

# These layers are the most the most likely to change during dev and
# are kept at the bottom for build time optimization.
COPY run.py $FLYWHEEL/
COPY fw_gear_nifti_to_mips $FLYWHEEL/fw_gear_nifti_to_mips

COPY pyproject.toml poetry.lock ./
RUN poetry install --no-dev


# Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py
ENTRYPOINT ["poetry","run","python","/flywheel/v0/run.py"]


