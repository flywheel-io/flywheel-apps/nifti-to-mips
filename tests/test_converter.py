import os
from pathlib import Path
from unittest import mock

import numpy as np
import pytest
from numpy.testing import assert_array_equal

from fw_gear_nifti_to_mips import converter

OUTPUT_DIR = (Path(__file__).parent / "output/").resolve()
ASSETS_DIR = (Path(__file__).parent / "assets/").resolve()


@pytest.fixture
def example_image_slice_data():
    return np.array([[1, 4, 5], [2, 3, 1]])


@pytest.fixture
def example_image_data():
    return np.array(
        [
            [[5, 9], [3, 6], [3, 8], [6, 2]],
            [[12, 6], [9, 55], [3, 4], [4, 4]],
            [[23, 7], [2, 4], [6, 5], [8, 6]],
            [[1, 2], [7, 5], [6, 9], [8, 5]],
        ]
    )


def test_set_min_max_threshold_value(example_image_slice_data):
    expected_res = np.array([[1, 4, 4], [2, 3, 1]])

    actual_res = converter.set_min_max_threshold_value(example_image_slice_data, 98.5)

    assert_array_equal(actual_res, expected_res)


def test_set_min_max_threshold_value_with_invalid_value(
    example_image_slice_data,
):

    with pytest.raises(ValueError) as execinfo:
        assert converter.set_min_max_threshold_value(example_image_slice_data, 123)

    with pytest.raises(Exception) as execinfo:
        assert converter.set_min_max_threshold_value(example_image_slice_data, "abc")


def test_scale_image_data(example_image_slice_data):
    expected_res = np.array([[0, 191, 255], [63, 127, 0]]).astype(np.uint8)

    actual_res = converter.scale_image_data(example_image_slice_data)

    assert_array_equal(actual_res, expected_res)


def test_create_mips_file_name():
    test_filename_1 = "abc.nii.gz"
    test_filename_2 = "abcd..nii"
    test_filename_3 = "abcd.you.nii.dc"

    test_file_type = "example_MIPs.png"

    res1 = converter.create_mips_file_name(test_filename_1, test_file_type)
    res2 = converter.create_mips_file_name(test_filename_2, test_file_type)
    res3 = converter.create_mips_file_name(test_filename_3, test_file_type)

    assert res1 == "abc_example_MIPs.png"
    assert res2 == "abcd._example_MIPs.png"
    assert res3 == "abcd.you_example_MIPs.png"


def test_get_image_slices_data(example_image_data):
    expected_sagittal = np.array([[23, 9], [9, 55], [6, 9], [8, 6]], np.int64)
    expected_coronal = np.array([[6, 9], [12, 55], [23, 7], [8, 9]], np.int64)
    expected_axial = np.array(
        [[9, 6, 8, 6], [12, 55, 4, 4], [23, 4, 6, 8], [2, 7, 9, 8]], np.int64
    )

    (
        actual_sagittal,
        actual_coronal,
        actual_axial,
    ) = converter.get_image_slices_data(example_image_data)

    assert_array_equal(actual_sagittal, expected_sagittal)
    assert_array_equal(actual_coronal, expected_coronal)
    assert_array_equal(actual_axial, expected_axial)


def test_process_mips(example_image_slice_data):
    actual_img = converter.process_mips(example_image_slice_data, 98.5, True)

    assert actual_img.mode == "L"


def test_check_nifti_dimension():
    example_tuple_one = (23, 12, 2)
    example_tuple_two = (23, "as", 2)
    example_tuple_three = (12, 23, 233, 23)

    res_one = converter.check_nifti_dimension(example_tuple_one)
    res_two = converter.check_nifti_dimension(example_tuple_two)
    res_three = converter.check_nifti_dimension(example_tuple_three)

    assert res_one
    assert not res_two
    assert not res_three


def test_main_with_valid_nifti_file(tmpdir):

    f1 = tmpdir.mkdir("subdir")

    nifti_file = f"{ASSETS_DIR}/image_1.nii.gz"

    converter.main(f1, nifti_file, "image_1.nii.gz", 98.5, True)


def test_main_with_invalid_nifti_image_shape(tmpdir):

    f1 = tmpdir.mkdir("subdir")

    nifti_file = f"{ASSETS_DIR}/aliza_ge.nii.gz"

    with pytest.raises(SystemExit) as e:
        converter.main(f1, nifti_file, "aliza_ge.nii.gz", 98.5, True)
    assert e.type == SystemExit
    assert e.value.code == 1


def test_main_with_invalid_output_file_path():

    nifti_file = f"{ASSETS_DIR}/image_1.nii.gz"

    with pytest.raises(SystemExit) as e:
        converter.main(OUTPUT_DIR, nifti_file, "image_1.nii.gz", 98.5, True)
    assert e.type == SystemExit
    assert e.value.code == 1


def test_main_with_invalid_nifti_file_path(tmpdir):

    f1 = tmpdir.mkdir("subdir")

    with pytest.raises(SystemExit) as e:
        converter.main(f1, ASSETS_DIR, "test-name", 98.5, True)
    assert e.type == SystemExit
    assert e.value.code == 1
