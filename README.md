# NIfTI To Mips
NIfTI To Mips is a utility gear that convert NIfTI file into PNG images by utilizing Maximum Intensity Projection(MIP) technique.

## Usage

### Inputs
#### Required
* __nifti_input_file__: Main input file for the Gear. A NIfTI file.

### Configuration

* __threshold_percentile__ (float, default 98.5): The percentile at which to threshold maximum values for MIP.
* __invert_image__ (boolean, default True): Inversion of the output PNG image.
* __debug__ (boolean, default False): Include debug statements in output.

