#!/usr/bin/python3
"""Gear entrypoint."""
import logging

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_nifti_to_mips import converter, parser

log = logging.getLogger(__name__)


if __name__ == "__main__":

    with GearToolkitContext() as gear_context:
        gear_context.init_logging()
        (
            filepath,
            filename,
            threshold_percentile,
            invert_image,
        ) = parser.parse_config(gear_context)
        converter.main(
            gear_context.output_dir,
            filepath,
            filename,
            threshold_percentile,
            invert_image,
        )
